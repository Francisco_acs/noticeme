import { ChangeEvent, FormEvent, useState } from "react";
import Header from "../../components/Header/Header";
import Tags from "../../components/Tags/Tags";
import { useAuth } from "../../context/ApiContext";
import { postPost } from "../../server/conectBack";
import { Btn, Container, Main, TextBox, TitleCss } from "./style";

function PostCreation() {
    const { userId, token } = useAuth()
    const [form, setForm] = useState({
        title: "",
        text:"",
        image_url:"",

    }); 

    const handleForm = (e: ChangeEvent) => {

        const target = e.target as HTMLTextAreaElement
        const { value, name } = target

        setForm({ ...form, [name]: value })
    }

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        console.log(form);
        postPost(token, {...form, user_id: userId} ).then((e)=> {console.log(e.data)}).catch((e) => {console.log(e.response.data)});
    }

    return ( 
        <Main>
            <Header />
            <Container>
                <form onSubmit={handleSubmit} >
                    <TitleCss  type="text" name="title" placeholder="Título" onChange={handleForm} required ></TitleCss>
                    <TitleCss  type="url" name="image_url" placeholder="URL da imagem" onChange={handleForm} required ></TitleCss>
                    <TextBox  name="text" onChange={handleForm}  placeholder="Texto" required ></TextBox>
                <div>
                    <ul>
                        <Tags />
                        <Tags />
                        <Tags />
                    </ul>

                    <Btn type="submit">Post</Btn>
                </div>
                </form>
                
                
            </Container>
        </Main> 
    );
}

export default PostCreation;