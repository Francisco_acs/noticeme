import { useEffect, useState } from "react";
import Header from "../../components/Header/Header";
import ProfilePost from "../../components/ProfilePosts/ProfilePost";
import { getUserData } from "../../server/conectBack";
import { ProfileCss } from "./style";
import { useAuth } from '../../context/ApiContext'
import { AiOutlineEdit } from "react-icons/ai";
import { useNavigate } from "react-router-dom";

export default function Profile() {
    const navigate = useNavigate()


    const { userId, token } = useAuth()
    const [data, setData] = useState({
        birthday: "",
        description: [{text:''}],
        email: "",
        id: 0,
        image_url: "",
        name: "",
        posts: []
    })

    useEffect(() => {

        getUserData(userId, token).then((e) => {
            setData(e.data)
        }).catch((e) => {
            alert(e.response.data)
        })

    }, [])

    console.log(data)

    return (
        <ProfileCss>
            <Header />
            <section>
                <nav>
                    <img src={data.image_url} alt='image' />
                    <h1>{data.name}</h1>
                </nav>

                <span>{data.description[0] ? data.description[0].text : ''}</span>
                <AiOutlineEdit onClick={() => navigate('/adminprofile')}/>
                <button>Follow</button>
            </section>
            <ul>
                {data.posts.map((e, i) => {
                    return <ProfilePost key={i} post = {e}/>
                })}
            </ul>
        </ProfileCss>
    );
}