import AdminPost from "../../components/AdminPost/AdminPost";
import Header from "../../components/Header/Header";
import { AdminCss } from "./style";
import { FiEdit2 } from 'react-icons/fi'
import { useAuth } from '../../context/ApiContext';
import { useEffect, useState } from "react";
import { getUserData, postDescription } from "../../server/conectBack";
import { AiOutlineCheck, AiOutlinePlus } from "react-icons/ai";


function AdminProfile() {
    const { userId, token } = useAuth()
    const [data, setData] = useState({
        birthday: "",
        description: [{text:''}],
        email: "",
        id: 0,
        image_url: "",
        name: "",
        posts: []
    })

    const [ description, setDescription ] = useState('')

    useEffect(() => {

        getUserData(userId, token).then((e) => {
            setData(e.data)
            setDescription(e.data.description[0].text)
        }).catch((e) => {
            console.log(e.response.data)
        })

    }, [])

    const checkPost = () => {
        postDescription(token, { description }).then((e)=> {console.log(e.data)}).catch((e) => {console.log(e.response.data)})
    }

    return ( 
        <AdminCss>
            <Header />
            <section>
                <nav>
                    <img src={data.image_url} alt='Imagem do usuário' />
                    <h1>{data.name}</h1>
                </nav>

                <span>{data.description[0] ? data.description[0].text : ''}</span>

                <textarea value={description} onChange={(e) => setDescription(e.target.value) } ></textarea>
                <div>
                    <FiEdit2 />
                    <AiOutlinePlus onClick={ checkPost } />
                    <AiOutlineCheck onClick={ checkPost } />
                </div>
                
            </section>
            <ul>
            {data.posts[0] ? data.posts.map((e, i) => {
                    return <AdminPost key={i} post={e} />
                }): <h1>Não tem post</h1>}

            </ul>
        </AdminCss>
     );
}

export default AdminProfile;