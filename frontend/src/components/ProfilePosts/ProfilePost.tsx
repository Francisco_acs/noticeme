import { useEffect, useState } from "react";
import { ProfilePostCss } from "./style";
import { BsHeart } from 'react-icons/bs'
import { TfiCommentAlt } from 'react-icons/tfi'
import { SlShare } from 'react-icons/sl'





export default function ProfilePost({ post }: any){

    return (
        <ProfilePostCss>

            <div>
                <h2>{post.title}</h2>
                <span>{post.text}</span>
            </div>
            <div>
                <img src={post.image_url} alt="post image" />
                <article>
                    <BsHeart  />
                    <TfiCommentAlt />
                    <SlShare />
                </article>
            </div>
        </ProfilePostCss>
    );

}